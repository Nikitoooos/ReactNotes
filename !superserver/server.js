import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use( function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');
    next();
});

app.get('/notes', function(request, response) {
	fs.readFile('nodes.json', 'utf8', function(error, data) {
        if(error) throw new Error('ups!'); 
        let toSend = JSON.parse(data);
        response.send(toSend);
	});
});

app.post('/notes', function(request, response) {
	let gotNotes = request.body;
	let toFile = JSON.stringify(gotNotes);
	fs.writeFile('nodes.json', toFile);
	response.status(200);
});

app.listen(5050);
console.log('Server is runing at port 5050');

