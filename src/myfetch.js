let MyFetch = {
	dataGet: (url) => {
		return new Promise( function(resolve, reject) {
			let req = new XMLHttpRequest();
			req.onload = () => {
				let resp = req.response;
				resolve(resp);
			}
			req.onerror = () => {
				class MyError extends Error {
					constructor(props) {
						super(props);
						this.name = 'ServerError';
						this.status = 500;
					}
				}
				let err = new MyError();
				reject(err);
			}
			req.open('GET', url, true);
			req.responseType = 'json';
			req.send();
		});
	},
	dataPost: (url, data) => {
		let req = new XMLHttpRequest();
		req.open('POST', url, true);
		req.setRequestHeader('Content-Type', 'application/json');
		req.send(JSON.stringify(data));
	}
}

export default MyFetch;
