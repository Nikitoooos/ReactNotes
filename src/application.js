import React from 'react';
import NotesList from './notes.js';
import MyFetch from './myfetch.js';
import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { CirclePicker } from 'react-color';

class TypoBoard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			text: '',
			noteArr: [],
			date: '',
			background: '#eedd82',
			checkSpinner: true,
			error: false,
			savedArr: [],
		} 
	}
	idCreater() {
		if(this.state.noteArr.length) {
			this.id = this.state.noteArr[0/*this.state.noteArr.length - 1*/].id + 1;
		} else {
			this.id = this.id || 0;
		}console
		return this.id
	}
	forDate() {
		let date = new Date();
		let day = date.getDate();
		let month = 1 + date.getMonth();
		if(day < 10) day = `0${day}`;
		if(month < 10) month = `0${month}`;
		let currentDate = `${day}.${month}.${date.getFullYear()}`;
		this.setState({date: currentDate});
	}
	getText(e) {
		let txt = e.target.value;
		this.setState({text: txt});
	}
	writeDownText() {
		let arr = this.state.noteArr;
		let textObj = {
			newtext: this.state.text || 'you may note all you want',
			date: this.state.date,
			id: this.idCreater(),
			background: this.state.background
		}
		arr.unshift/*push*/(textObj);
		this.setState({noteArr: arr, text: ''});
	}
	copyWDT(e) {
		if(e.key == 'Enter') {
			this.writeDownText();
		}
	}
	noteEditor(index, text) {
		let arr = this.state.noteArr;
		arr[index].newtext = text;
		this.setState({noteArr: arr});
	}
	clearFunc(index) {
		let arr = this.state.noteArr;
		arr.splice(index, 1);
		this.setState({noteArr: arr, checkSpinner: false});
	}

	componentWillMount() {
		this.forDate();
		MyFetch.dataGet('http://localhost:5050/notes').then( (response) => {
			setTimeout(() => {
				this.setState({noteArr: response});
					if(response.length == 0) {
					this.setState({checkSpinner: false});
				}
			}, 1500);
		}, (err) => {
			if(err.status == 500) {
				let maindiv = this.refs.MainDiv;
				maindiv.classList.toggle('dispnon');
				this.setState({error: true, checkSpinner: false}); 
			}
		});
	}
	componentDidMount() {
		window.addEventListener('scroll', this.scrTop.bind(this));
		this.linkControl();
	}
	linkControl() {
		let links = this.refs.Footer.getElementsByTagName('a');
		for(let i = 0; i < links.length; i++) {
			links[i].addEventListener('click', (e) => {e.preventDefault()});
		}
	}
	scrTop() {
		let fromTop = document.body.scrollTop;
		let arrowup = this.refs.ArrowUp;
		if(fromTop >= 150) {
			arrowup.classList.remove('dispnon');
		} else {
			arrowup.classList.add('dispnon');
		}
	}
	toTop() {
		let top = document.body.scrollTop;
		let interval = setInterval( () => {
			top = top - 4;
			window.scrollTo(0, top);
			if(top <= 0) {
				clearInterval(interval);
			}
		}, 3);
	}
	saveNotes() {
		MyFetch.dataPost('http://localhost:5050/notes', this.state.noteArr);
	}
	changeColor(color) {
		this.setState({background: color.hex});
	}
	colorEditor(index, color) {
		let arr = this.state.noteArr;
		arr[index].background = color;
		this.setState({noteArr: arr});
	}
	// arraySorting(e) {
	// 	let index = e.target.selectedIndex;
	// 	let arr = this.state.noteArr;
	// 	if(index == 1) {
	// 	arr.sort( (a, b) => {
	// 			if (a.date == b.date) {
	// 				return 0 
	// 			} else if(a.date < b.date) {
	// 				return 1
	// 			} else {
	// 				return -1
	// 			}
	// 		}); 
	// 	} else if(index == 2) {
	// 		arr.sort( (a, b) => {
	// 			if(a.date == b.date) {
	// 				return 0
	// 			} else if(a.date > b.date) {
	// 				return 1
	// 			} else {
	// 				return -1
	// 			}
	// 		});
	// 	}
	// 	this.setState({noteArr: arr});
	// }
	finder(e) {
		let text = e.target.value;
		let arr = this.state.noteArr, svArr = this.state.savedArr;;
		let nomatches = {};
		let newArr = arr.filter( (item) => {
			let cheker = item.newtext.toLowerCase().match(/[A-Za-z\d]+\b/g);
			for(let i = 0; i < cheker.length; i++) {
				if(cheker[i] == text) {
					return item
				} else {
					nomatches.clear = true;
				} 
			}
		})
		if(svArr.length == 0) {
			svArr = [...arr];
		}
		if(newArr.length != 0) {
			this.setState({noteArr: newArr, savedArr: svArr});
		} else if (text == '' || nomatches.clear == true) {
			this.setState({noteArr: svArr});
		}
	}

	render() {
		let notes;
		if(this.state.noteArr.length) {
			notes = this.state.noteArr.map( (item, index) => {
				return <NotesList itemtext={item.newtext} typoDate={item.date} key={item.id} index={index}
				currentColor={item.background} typoEditor={this.noteEditor.bind(this)} 
				clearOne={this.clearFunc.bind(this)} stateColor={this.state.background} newColor={this.colorEditor.bind(this)} />
			}); 
		} else if(this.state.checkSpinner) {
			notes = (<div className="forloader">
						<span className="fa fa-spinner fa-pulse fa-4x"></span>
					</div>);
		} else if(this.state.error) {
			notes = (<div className="error">
						<p>There's something wrong with the server!</p>
					</div>)
		}
		return (
			<div>
				<div className="subMain">
					<div className="row mainrow" ref="MainDiv">
						<input className="form-control col-sm-4 maininp" onKeyPress={this.copyWDT.bind(this)} onChange={this.getText.bind(this)}
						placeholder="You can create your note here" value={this.state.text} />
						<input className="form-control col-sm-3 maininp" placeholder="And find here..." onChange={this.finder.bind(this)} />
						<div className="col-sm-5 buttons">
							<button className="btn btn-success text-center" onClick={this.writeDownText.bind(this)}>ADD</button>
							<button className="btn btn-danger danger" onClick={this.saveNotes.bind(this)}><i className="fa fa-plus"></i></button>
							<div className="btn-group">
								<button className="btn btn-info dropdown-toggle" data-toggle="dropdown">Colors</button>
								<div className="dropdown-menu">
									<CirclePicker color={this.state.background} onChangeComplete={this.changeColor.bind(this)} />
								</div>
							</div>
						</div>
					</div>
					<div className="dispnon arrowUp" ref="ArrowUp">
						<div className="circ" onClick={this.toTop.bind(this)}>
							<i className="fa fa-angle-double-up"></i>
						</div>
					</div>
					<div>
						<ReactCSSTransitionGroup transitionName="slowlyNotes" transitionEnterTimeout={330} transitionLeaveTimeout={330}>
							{notes}
						</ReactCSSTransitionGroup>
					</div>
					<div className="ccb"></div>
				</div>
				<div className="footer" ref="Footer">
					<div>
						<p>Copyright 2017 Pobazhak N. All rights reserved.</p>
						<p><a href="#">Contacts</a> | <a href="#">Support</a> | <a href="#">Docs</a></p>
					</div>
				</div>
			</div>
			)
	}
}
export default TypoBoard;