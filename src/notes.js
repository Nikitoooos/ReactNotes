import React from 'react';

class NotesList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			formEdition: false,
			background: '#eedd82'
		}
	}
	switchEdit() {
		if(this.props.itemtext == '') {
			this.props.typoEditor(this.props.index, 'empty note');
		}
		this.setState({formEdition: !this.state.formEdition});
	}
	copySE(e) {
		if(e.key == 'Enter') {
			this.switchEdit();
		}
	}
	showDiv() {
		return (
				<div className="points" onClick={this.switchEdit.bind(this)}>
					{this.props.itemtext}
					<p className="date">{this.props.typoDate}</p>
				</div>
			)
	}
	forTextarea() {
		return (
			<div>
				<textarea className="form-control" onKeyPress={this.copySE.bind(this)} 
				onChange={this.textWatch.bind(this)} value={this.props.itemtext}></textarea>
				<button className="btn btn-danger btn-sm cross" 
				onClick={this.clearFunc.bind(this)}><i className="fa fa-times"></i></button>
				<button className="btn btn-warning btn-sm brush" onClick={this.colorise.bind(this)}
				><i className="fa fa-paint-brush"></i></button>
			</div>
			)
	}
	clearFunc() {
		this.props.clearOne(this.props.index);
	}
	textWatch(e) {
		let text = e.target.value;
		this.props.typoEditor(this.props.index, text);
	}
	async checkColor(elem) {
		await this.colorise;
		if(this.state.background == '#eedd82') {
			
		}
	}
	async forColor() {
		await this.colorise;
		this.props.newColor(this.props.index, this.state.background);
	}
	colorise(e) {
		this.setState({background: this.props.stateColor});
		this.checkColor(e.target);
		this.forColor();
	}

	render() {
		let innerpar;
		if(this.state.formEdition) {
			innerpar = this.forTextarea();
		} else {
			innerpar = this.showDiv();
		}
		return ( 
			<div className="wrap" style={{background: this.props.currentColor, boxShadow: '0 0 7px 3px' + this.props.currentColor}}>
				{innerpar}
			</div>
		)	
	}
}

export default NotesList;
