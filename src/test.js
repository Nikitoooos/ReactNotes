import React from 'react';

class Superexample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  render() {
    return (
      <div>
          <a href="#myModal" className="btn btn-primary" data-toggle="modal">Open modal</a>
          <div id="myModal" className="modal fade">
              <div className="modal-dialog">
                  <div className="modal-content">
                      <div className="modal-header">
                        <h4 className="modal-title">Заголовок модального окна</h4>
                      </div>
                      <div className="modal-body">
                        Содержимое модального окна...
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-default" data-dismiss="modal">OK</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}
export default Superexample;